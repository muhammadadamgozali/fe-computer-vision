// import React, { useState } from 'react';
import styles from './dialogcontent.module.css';
import { useRouter } from 'next/router';
import InputField from '../InputField/InputField';
import Button from '../Button';
// import api from '../../../utils/api';
// import { useDispatch } from 'react-redux';
// import { setShowAlert, setAlertMessage, setAlertType } from '../Alert/action';

const LoginDialogContent = () => {
  // const dispatch = useDispatch();
  const router = useRouter();
  // const [form, setForm] = useState({
  //   password: '',
  //   username: '',
  // });
  // const handleSubmit = async (e) => {
  //   e.preventDefault();
  //   const body = form;
  //   try {
  //     const res = await api.post('/users/v1/login', body, {
  //       auth: {
  //         password: process.env.NEXT_PUBLIC_BASIC_AUTH_PASSWORD,
  //         username: process.env.NEXT_PUBLIC_BASIC_AUTH_USERNAME,
  //       } });
  //     localStorage.setItem('token', res?.data?.data);
  //     localStorage.setItem('username', form.username);
  //     router.push('/');
  //   }
  //   catch (err) {
  //     dispatch(setShowAlert(true));
  //     dispatch(setAlertMessage(err?.response?.data?.message || err?.message));
  //     dispatch(setAlertType('error'));
  //   }
  // };
  return (
    <div className={styles['dialog-content']}>
        <span className={styles['dialog-content__title-text']}>LOGIN</span>
      <form
        className={styles['dialog-content__form']}
        // onSubmit={handleSubmit}
      >
        <div style={{ marginTop: '40px' }} />
        <InputField
          label="Username"
          type="text"
          // onChange={(e) => setForm({ ...form, username: e.target.value })}
          placeholder="Enter your username"
          // value={form.username}
        />
        <div style={{ marginTop: '20px' }} />
        <InputField
          label="Password"
          // onChange={(e) => setForm({ ...form, password: e.target.value })}
          placeholder="Enter your Password"
          type="password"
          // value={form.password}
        />
        <div style={{ marginTop: '5px' }} />
        <div className={styles['dialog-content__other-options']}>
          <div className={styles['dialog-content__forgot-password']}>
            Forgot password?
          </div>
        </div>
        <div style={{ marginTop: '20px' }} />
        <div className={styles['dialog-content__submit-btn']}>
          <Button buttonType="primary" fullWidth type="submit">
            Login
          </Button>
        </div>
        <div style={{ marginTop: '10px' }} />
        <div className={styles['dialog-content__redirect']}>
          <div className={styles['dialog-content__redirect-text']}>
            Don&apos;t have account yet?{' '}
            <span
              className={styles['dialog-content__redirect-link']}
              onClick={() => router.push('/register')}
            >
              Sign Up
            </span>
          </div>
        </div>
      </form>
    </div>
  );
};

export default LoginDialogContent;
