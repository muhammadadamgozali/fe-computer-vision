import React, { useState } from 'react';
import styles from './dialogcontent.module.css';
import { useRouter } from 'next/router';
import InputField from '../InputField/InputField';
import Button from '../Button/Button';
// import api from '../../../utils/api';
// import { useDispatch } from 'react-redux';
// import { setShowAlert, setAlertMessage, setAlertType } from '../Alert/action';

const RegisterDialogContent = () => {
  // const dispatch = useDispatch();
  const router = useRouter();
  const [form, setForm] = useState({
    email: '',
    password: '',
    phone: '',
    username: '',
  });
  const handleSubmit = async (e) => {
    e.preventDefault();
    const body = form;
    try {
      await api.post('/users/v1/register', body, {
        auth: {
          password: process.env.NEXT_PUBLIC_BASIC_AUTH_PASSWORD,
          username: process.env.NEXT_PUBLIC_BASIC_AUTH_USERNAME,
        } });
      dispatch(setShowAlert(true));
      dispatch(setAlertMessage('Registered successfully, please check your email to verify your account'));
      dispatch(setAlertType('success'));
      router.push('/login');
    }
    catch (err) {
      dispatch(setShowAlert(true));
      dispatch(setAlertMessage(err?.response?.data?.message || err?.message));
      dispatch(setAlertType('error'));
    }
  };
  return (
    <div className={`${styles['dialog-content']} ${styles['dialog-content--login']}`}>
      
        <span className={styles['dialog-content__title-text']}>Sign Up</span>
      <form
        className={styles['dialog-content__form']}
        onSubmit={handleSubmit}
      >
        <div style={{ marginTop: '40px' }} />
        <InputField
          label="Email"
          onChange={(e) => setForm({ ...form, email: e.target.value })}
          placeholder="Enter your email"
          type="email"
          value={form.email}
        />
        <div style={{ marginTop: '20px' }} />
        <InputField
          label="Username"
          onChange={(e) => setForm({ ...form, username: e.target.value })}
          placeholder="Enter your username"
          value={form.username}
        />
        <div style={{ marginTop: '20px' }} />
        <InputField
          label="Password"
          onChange={(e) => setForm({ ...form, phone: e.target.value })}
          placeholder="Enter your phone number"
          type="password"
          value={form.phone}
        />
        <div style={{ marginTop: '20px' }} />
        <InputField
          label="Re-type Password"
          onChange={(e) => setForm({ ...form, password: e.target.value })}
          placeholder="Enter your Password"
          type="password"
          value={form.password}
        />
        <div style={{ marginTop: '40px' }} />
        <div className={styles['dialog-content__submit-btn']}>
          <Button buttonType="primary" fullWidth type="submit">
            Register
          </Button>
        </div>
        <div style={{ marginTop: '40px' }} />
        <div className={styles['dialog-content__redirect']}>
          <div className={styles['dialog-content__redirect-text']}>
            Already have an Account?{' '}
            <span
              className={styles['dialog-content__redirect-link']}
              onClick={() => router.push('/login')}
            >
              Login
            </span>
          </div>
        </div>
      </form>
    </div>
  );
};

export default RegisterDialogContent;
