import React from "react";
import styles from "./comparrationtraffic.module.css";
import DatePicker from "../Date/Date";

const ComparrationTraffic = () => {
  return (
    <div className={styles["comparration"]}>
      <div className={styles["comparration__content-left"]}>
        <div className={styles["comparration__title"]}>
          Comparration Traffic
        </div>
        <div
          className={styles["gate__right"]}
          // onClick={backTool}
        >
          Gate <span className="text-green-500">(in) <i class="fa-solid fa-arrow-right-long" /></span>
        </div>
        <div className="text-xl">1000</div>
      </div>
      <div className={styles["comparration__content-right"]}>
      <h2><DatePicker/></h2>
      <div
          className={styles["gate__left"]}
          // onClick={backTool}
        >
          Gate <span className="text-red-500">(out) <i class="fa-solid fa-arrow-left-long" /></span>
        </div>
        <div className="text-xl">1000</div>
      </div>
    </div>
  );
};

export default ComparrationTraffic;
