import React, { useState } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import styles from "./date.module.css"
import { Icon } from '@iconify-icon/react';

const DatePicker = () => {
  const [date, setDate] = useState(new Date());
  const [showCalendar, setShowCalendar] = useState(false);

  const onChange = date => {
    setDate(date);
    setShowCalendar(false);
  };

  const toggleCalendar = () => {
    setShowCalendar(!showCalendar);
  };

  return (
    <div className={styles.calendarContainer}>
      <p onClick={toggleCalendar}>{date.toDateString()} <Icon icon="solar:calendar-linear" /></p>
      {showCalendar && (
        <div className={styles.popupCalendar}>
          <Calendar onChange={onChange} value={date} />
        </div>
      )}
    </div>
  );
};

export default DatePicker;
