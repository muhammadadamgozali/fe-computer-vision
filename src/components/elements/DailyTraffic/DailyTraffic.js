import React from "react";
import styles from "./dailytrafic.module.css";
import { Icon } from "@iconify-icon/react";


const DailyTraffic = () => {
  return (
    <div className={styles["dailytraffic"]}>
      <h1 className={styles["dailytraffic__title"]}>Daily Traffic</h1>
      <div className={styles["dailytraffic__icon"]}>
        <Icon icon="healthicons:cross-country-motorcycle" className={styles.icon} />1000
        <Icon icon="fluent:vehicle-car-profile-ltr-16-filled" className={styles.icon} />1000
        <Icon icon="mingcute:bus-2-line" className={styles.icon} />1000
        <Icon icon="mdi:truck" className={styles.icon} />1000
        <Icon icon="clarity:bicycle-solid" className={styles.icon} />1000
      </div>
    </div>
  );
};

export default DailyTraffic;
