import React from 'react';
import styles from './button.module.css';
import PropTypes from 'prop-types';

const Button = (props) => {
  const {
    children,
    fullWidth,
    buttonType,
    classnames,
    ...attributes
  } = props;
  return (
    <button
      className={`${styles[`btn--${buttonType}`]} ${fullWidth ? styles['full-width'] : ''} ${classnames}`}
      {...attributes}
    >
      {children}
    </button>
  );
};

export default Button;

Button.propTypes = {
  buttonType: PropTypes.oneOf(['primary', 'secondary', 'outline']),
  children: PropTypes.node.isRequired,
  classnames: PropTypes.string,
  fullWidth: PropTypes.bool,
};

Button.defaultProps = {
  buttonType: 'primary',
  classnames: '',
  fullWidth: false,
};
