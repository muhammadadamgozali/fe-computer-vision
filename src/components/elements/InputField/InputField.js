import React, { useState } from 'react';
import { RiEyeCloseLine, RiEyeLine } from 'react-icons/ri';
import styles from './inputfield.module.css';
import PropTypes from 'prop-types';

const InputField = (props) => {
  const {
    label,
    fullWidth,
    isMulti,
    type,
    ...attributes
  } = props;

  const [showPassword, setShowPassword] = useState(false);

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  return (
    <div className={`${styles['input-field']} ${fullWidth ? styles['full-width'] : ''}`}>
      {label && <label className={styles['input-field__label']}>{label}</label>}
      {isMulti ? (
        <textarea {...attributes} className={styles['input-field__textarea']} />
      ) : (
        <div className={styles['input-field__wrapper']}>
          <input
            {...attributes}
            type={type === 'password' && showPassword ? 'text' : type}
            className={styles['input-field__input']}
          />
          {type === 'password' && (
            <span
              className={styles['input-field__toggle']}
              onClick={togglePasswordVisibility}
            >
              {showPassword ? <RiEyeLine /> : <RiEyeCloseLine />}
            </span>
          )}
        </div>
      )}
    </div>
  );
};

export default InputField;

InputField.propTypes = {
  fullWidth: PropTypes.bool,
  isMulti: PropTypes.bool,
  label: PropTypes.string,
  type: PropTypes.string,
};

InputField.defaultProps = {
  fullWidth: false,
  isMulti: false,
  label: '',
  type: 'text',
};
