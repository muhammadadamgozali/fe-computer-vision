import { useState, useEffect, useRef, React } from 'react';
import styles from './navbar.module.css';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import Image from 'next/image';

const Navbar = (props) => {
  const ref = useRef(null);
  const { navitems } = props;
  const router = useRouter();
  const [showUserMenu, setShowUserMenu] = useState(false);

  const isHome = router.pathname === '/';

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (ref.current && !ref.current.contains(event.target)) {
        setShowUserMenu(false);
      }
    };
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [ref]);

  const handleLogout = () => {
    localStorage.removeItem('token');
    router.push('/login');
  };

  return (<><div className={styles['navbar']}>
    <div className={styles['navbar__left-segment']}>
        <Image alt="logo telkom" height={150} src="/assets/images/telkomai-logo.svg" width={150} />  
    </div>
    <div className={styles['navbar__center-segment']}>
      <div className={styles['tab-container']}>
        {
          navitems.map((nav, index) => {
            return (<div
              className={styles['tab']}
              key={index}
            >
              <div className={`${styles['inner-tab']} ${nav.active ? styles['selected'] : ''}`}
                onClick={() => {
                  router.push(nav.url);
                }}>
                <Image alt="icon-chat" height={37} src={nav.icon} width={20} />{nav.title}
              </div>
            </div>);
          })
        }
      </div>
    </div>
    <div className={styles['navbar__right-segment']}>
      <Image
        alt="no-avatar"
        className={styles['avatar']}
        height={37}
        onClick={() => setShowUserMenu(!showUserMenu)}
        src="/assets/images/no-avatar.svg"
        width={37}
      />
      {showUserMenu && (
        <div className={styles['navbar__user-menu-popup']} onClick={() => {setShowUserMenu(false);
          handleLogout();}} ref={ref}>
          <div className={styles['navbar__user-menu-popup-item']}>
            <span>Logout</span>
            <i className="fa-solid fa-sign-out" />
          </div>
        </div>
      )}
    </div>
  </div></>);
};

export default Navbar;

Navbar.propTypes = {
  navbarType: PropTypes.oneOf(['home', 'project']),
  navitems: PropTypes.array
};

Navbar.defaultProps = {
  navbarType: 'home',
  navitems: []
};
