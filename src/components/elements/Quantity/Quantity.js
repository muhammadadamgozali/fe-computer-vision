import React from "react";
import styles from "./quantity.module.css"

const Quantity = () => {
  return (
    <div className={styles["quantity"]}>
      <div className={styles["quantity__content-left"]}>
        <div className={styles["left__title"]}>
          Sunday
        </div>
        <div
          className={styles["gate__right"]}
          // onClick={backTool}
        >
          Total Quantity
        </div>10000
      </div>
      <div className={styles["quantity__content-right"]}>
         <div className={styles["right__title"]}>
          Monday
        </div>
        <div
          className={styles["get__left"]}
          // onClick={backTool}
        >
          Total Quantity
        </div>10000
      </div>
    </div>
  );
};

export default Quantity;
