import React from 'react'
import styles from './trashManagement.module.css'

const TrashManagement = () => {
  return (
    <div className={styles['trash']}>
      <h1>Trash Management</h1>
    </div>
  )
}

export default TrashManagement
