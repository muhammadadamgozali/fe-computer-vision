import React from 'react'
import styles from './licenseRecognition.module.css'

const LicenseRecognition = () => {
  return (
    <div className={styles["license"]}>
     <h1>License Recognition</h1>
    </div>
  )
}

export default LicenseRecognition;
