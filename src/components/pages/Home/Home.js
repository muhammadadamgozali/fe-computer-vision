import styles from "./home.module.css";
import Link from "next/link";
import Button from "@/components/elements/Button";
// import { useEffect } from 'react';
// import { useRouter } from 'next/navigation';

export default function Home() {
  // const router = useRouter();

  //   useEffect(() => {
  //     router.push('/login');
  //   }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleNavigation = (url) => {
    window.open(url, "_blank");
  };

  return (
    <div className={styles["home"]}>
      <Button
        buttonType="outline"
        onClick={() => handleNavigation("/vehiclecounting")}
      >
        Vehicle Counting
      </Button>
      <Button
        buttonType="outline"
        onClick={() => handleNavigation("/licenserecognition")}
      >
        License Plate Recognition
      </Button>
      <Button
        buttonType="outline"
        onClick={() => handleNavigation("/trashmanagement")}
      >
        Trash Management System
      </Button>
      <Button
        buttonType="outline"
        onClick={() => handleNavigation("/crowddetection")}
      >
        Crowd Detection
      </Button>
      <Button
        buttonType="outline"
        onClick={() => handleNavigation("/surveillancesystem")}
      >
        Surveillance System
      </Button>
    </div>
  );
}
