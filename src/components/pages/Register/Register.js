import React, { useEffect } from "react";
import styles from "./register.module.css";
import Image from "next/image";
import RegisterDialogContent from "../../elements/Dialog/RegisterDialogContent";
// import { useRouter } from 'next/router';

const Register = () => {
  // const router = useRouter();
  // useEffect(() => {
  //   router.push('/login');
  // }, []); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <div className={styles["register"]}>
      <div className={styles["register__content"]}>
        <div className={styles["register__left-section"]}>
          <Image
            alt="telkomai-logo"
            className={styles["register__logo"]}
            height={200}
            layout="fixed"
            src="/assets/images/telkomai-logo.svg"
            width={200}
          />
          <Image
            alt="register-bg"
            className={styles["register__bg"]}
            height={846}
            src="/assets/images/login-pattern.svg"
            width={582}
          />
        </div>
        <div className={styles["register__right-section"]}>
          <div className={styles["register__dialog-container"]}>
            <RegisterDialogContent />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
