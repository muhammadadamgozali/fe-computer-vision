import React from "react";
import styles from "./vehicleCounting.module.css";
import DailyTraffic from "@/components/elements/DailyTraffic/DailyTraffic";
import ComparrationTraffic from "@/components/elements/ComparrationTraffic/ComparrationTraffic";
import Quantity from "@/components/elements/Quantity/Quantity";

const VehicleCounting = () => {
  return (
    <div className={styles["vehicle"]}>
      <div className={styles["vehicle__left-content"]}>
        <DailyTraffic />
        <ComparrationTraffic />
        <Quantity />
      </div>
      <div className={styles["vehicle__center-content"]}>Center Content</div>
      <div className={styles["vehicle__right-content"]}>Right Content</div>
    </div>
  );
};

export default VehicleCounting;
