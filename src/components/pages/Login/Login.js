import React from "react";
import styles from "./login.module.css";
import Image from "next/image";
import { LoginDialogContent } from "@/components/elements/Dialog";
// import Navbar from "@/components/Navbar/Navbar";


const Login = () => {
  return (
    <div className={styles["login"]}>
      <div className={styles["login__content"]}>
        <div className={styles["login__left-section"]}>
            <Image
              alt="telkomai-logo"
              className={styles["login__logo"]}
              height={200}
              src="/assets/images/telkomai-logo.svg"
              width={200}
            />
          <Image
            alt="login-bg"
            className={styles["login__bg"]}
            height={846}
            src="/assets/images/login-pattern.svg"
            width={582}
          />
        </div>
        <div className={styles["login__right-section"]}>
          <div className={styles["login__dialog-container"]}>
            <LoginDialogContent />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
