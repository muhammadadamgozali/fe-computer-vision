import React from 'react';
import PropTypes from 'prop-types';

import Navbar from '@/components/elements/Navbar/Navbar';
// import useNavbarConst from '../../../hooks/useNavbarConst';
export default function ProjectPage(props) {
  const { children } = props;
//   const { navProjectItems } = useNavbarConst();

  return (
    <div className="flex min-h-screen">
      <Navbar />
      <div className="pt-[55px] flex flex-1 justify-center">
        {children}
      </div>
    </div>
  );
}

ProjectPage.propTypes = {
  children: PropTypes.element,
};

ProjectPage.defaultProps = {
  children: null,
};
