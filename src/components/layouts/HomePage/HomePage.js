import React from 'react';
import PropTypes from 'prop-types';

import Navbar from '@/components/elements/Navbar/Navbar';
import ProjectPage from '../ProjectPage';
// import useNavbarConst from '../../../hooks/useNavbarConst';

export default function HomePage(props) {
  const { children } = props;
//   const { navHomeItems } = useNavbarConst();

  return (
    <div className="flex min-h-screen">
      <Navbar variant="home" />
      <div className="pt-[55px] flex flex-1">
        {children}
      </div>
    </div>
  );
}

HomePage.propTypes = {
  children: PropTypes.element,
};

HomePage.defaultProps = {
  children: null,
};
