import ProjectPage from '@/components/layouts/ProjectPage'
import CrowdDetection from '@/components/pages/CrowdDetection'
import React from 'react'

export default function index() {
  return (
   <ProjectPage>
    <CrowdDetection/>
   </ProjectPage>
  )
}
