import React from 'react'
import ProjectPage from '@/components/layouts/ProjectPage'
import TrashManagement from '@/components/pages/TrashManagement'

export default function index() {
  return (
    <ProjectPage>
     <TrashManagement/>
    </ProjectPage>
  )
}
