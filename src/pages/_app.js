import "@/styles/globals.css";
import { ThemeProvider } from "@emotion/react";
import { legionTheme } from "@legion-ui/core";

export default function App({ Component, pageProps }) {
  return (
  <ThemeProvider theme={legionTheme}>
    <Component {...pageProps} />
  </ThemeProvider>
  );
}
