import ProjectPage from '@/components/layouts/ProjectPage'
import SurveillanceSystem from '@/components/pages/SurveillanceSystem'
import React from 'react'

export default function index() {
  return (
   <ProjectPage>
    <SurveillanceSystem/>
   </ProjectPage>
  )
}
