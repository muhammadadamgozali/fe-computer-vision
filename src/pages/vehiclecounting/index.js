import ProjectPage from '@/components/layouts/ProjectPage'
import VehicleCounting from '@/components/pages/VehicleCounting'
import React from 'react'

export default function index() {
  return (
   <ProjectPage>
    <VehicleCounting/>
   </ProjectPage>
  )
}
