import Home from '@/components/pages/Home';
import HomeBase from '@/components/layouts/HomePage';
import React from 'react';

export default function Index() {
  return (
    <HomeBase>
      <Home />
    </HomeBase>
  );
}
