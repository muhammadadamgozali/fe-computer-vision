import React from 'react'
import ProjectPage from '@/components/layouts/ProjectPage'
import LicenseRecognition from '@/components/pages/LicenseRecognition'

export default function index() {
  return (
    <ProjectPage>
     <LicenseRecognition/>
    </ProjectPage>
  )
}
